package com.raven.android_lab4;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class GameOverActivity extends Activity implements View.OnClickListener
{
    TextView obstacleCounterTextView_;
    Button restartButton_;

    @Override
    public void onClick(View _view)
    {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }
    @Override
    protected void onCreate(Bundle _savedInstanceState)
    {
        super.onCreate(_savedInstanceState);
        setContentView(R.layout.activity_game_over);

        obstacleCounterTextView_ = findViewById(R.id.obstacleCounterTV);
        restartButton_ = findViewById(R.id.restartBT);
        restartButton_.setOnClickListener(this);

        obstacleCounterTextView_.setText("" + getIntent().getIntExtra("score", -1));
    }
}
