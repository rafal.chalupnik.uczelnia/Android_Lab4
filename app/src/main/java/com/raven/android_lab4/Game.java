package com.raven.android_lab4;

import java.util.Random;

public class Game
{
    private final int displayHeight_;
    private final int displayWidth_;
    private final int iconSize_;
    private int obstacleCounter_ = -1;
    private int obstaclePosition_ = -1;
    private boolean obstacleVisible_ = false;
    private int obstacleY_ = -1;
    private int playerPosition_ = 1;
    private int playerY_;
    private Random random_;
    private final int separator_;

    private int getFieldX(int _field)
    {
        return (separator_ * (_field + 1) + iconSize_ * _field) - iconSize_ / 2;
    }

    public Game(int _iconSize, int _displayWidth, int _displayHeight)
    {
        iconSize_ = _iconSize;
        displayWidth_ = _displayWidth;
        displayHeight_ = _displayHeight;

        separator_ = (displayWidth_ - iconSize_ * 3) / 4;
        playerY_ = displayHeight_ - 3 * iconSize_;
        random_ = new Random();
    }

    public boolean collisionOccured()
    {
        return obstaclePosition_ == playerPosition_ && playerY_ < obstacleY_ + iconSize_;
    }
    public int getVerticalAxis() { return displayWidth_ / 2; }
    public int getObstacleCounter() { return obstacleCounter_; }
    public int getObstacleX() { return getFieldX(obstaclePosition_); }
    public int getObstacleY() { return obstacleY_; }
    public int getPlayerX()
    {
        return getFieldX(playerPosition_);
    }
    public int getPlayerY() { return playerY_; }
    public void moveLeft()
    {
        if (playerPosition_ != 0)
            playerPosition_--;
    }
    public void moveObstacle()
    {
        if (obstacleVisible_)
        {
            obstacleY_ += 25;
            if (obstacleY_ > displayHeight_)
                obstacleVisible_ = false;
        }
        else
        {
            obstaclePosition_ = random_.nextInt(3);
            obstacleY_ = -iconSize_;
            obstacleVisible_ = true;
            obstacleCounter_++;
        }
    }
    public void moveRight()
    {
        if (playerPosition_ != 2)
            playerPosition_++;
    }
}