package com.raven.android_lab4;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;

public class GameView extends SurfaceView implements Runnable, View.OnTouchListener
{
    private Game game_;
    private SurfaceHolder holder_;

    public GameView(Context _context, int _displayWidth, int _displayHeight)
    {
        super(_context);
        holder_ = getHolder();

        game_ = new Game(150, _displayWidth, _displayHeight);
        setOnTouchListener(this);
    }

    public boolean draw()
    {
        if (holder_.getSurface().isValid())
        {
            Canvas canvas = holder_.lockCanvas();
            canvas.drawColor(Color.WHITE);

            // Draw player
            Bitmap player = BitmapFactory.decodeResource(this.getResources(), R.drawable.car);
            canvas.drawBitmap(player, game_.getPlayerX(), game_.getPlayerY(), new Paint());

            // Draw obstacle
            game_.moveObstacle();
            Bitmap obstacle = BitmapFactory.decodeResource(this.getResources(), R.drawable.obstacle);
            canvas.drawBitmap(obstacle, game_.getObstacleX(), game_.getObstacleY(), new Paint());

            // Check for collision
            if (game_.collisionOccured())
            {
                Intent intent = new Intent(getContext(), GameOverActivity.class);
                intent.putExtra("score", game_.getObstacleCounter());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getContext().startActivity(intent);
                return false;
            }

            holder_.unlockCanvasAndPost(canvas);
        }
        return true;
    }
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent)
    {
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
        {
            if (motionEvent.getX() < game_.getVerticalAxis())
                game_.moveLeft();
            else
                game_.moveRight();
        }

        return super.onTouchEvent(motionEvent);
    }
    @Override
    public void run()
    {
        if (draw())
            getHandler().postDelayed(this, 10);
    }
}