package com.raven.android_lab4;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.text.Layout;
import android.util.DisplayMetrics;
import android.view.View;

public class MainActivity extends Activity
{
    private Handler handler_;

    @Override
    protected void onCreate(Bundle _savedInstanceState)
    {
        super.onCreate(_savedInstanceState);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        GameView gameView = new GameView(getApplicationContext(), displayMetrics.widthPixels, displayMetrics.heightPixels);
        setContentView(gameView);

        handler_ = new Handler();
        handler_.postDelayed(gameView, 100);
    }
}
